package com.devcamp.task5440project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5440projectApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task5440projectApplication.class, args);
	}

}
