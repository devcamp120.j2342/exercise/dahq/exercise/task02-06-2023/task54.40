package com.devcamp.task5440project;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LuckiDice {
    @CrossOrigin
    @GetMapping("/dice")
    public String luckyDice(@RequestParam(required = true, name = "username") String userName) {
        int random = 1 + (int) (Math.random() * ((10 - 1) + 1));
        return "Xin chào: " + userName + ", Số may mắn hôm nay của bạn là số: " + random;
    }

    @CrossOrigin
    @GetMapping("/devcamp-date")
    public String getDateViet() {
        DateTimeFormatter dtfVietNam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        return String.format("Hello pizza Loverrr!! Hôm nay %s mua 1 tặng 1 mại dô", dtfVietNam.format(today));
    }

    @CrossOrigin
    @GetMapping("/abcc")
    public String Hoo() {
        return "hello";
    }

}
